import { GameScene, Line, Border} from './game-scene';
import { GameSceneExtra } from './game-bigScene';

export class Graph_board  {
    private graphics: Phaser.GameObjects.Graphics;
    private game: GameScene | GameSceneExtra;
    private radius:number;
    private stroke:number

    constructor(graphics: Phaser.GameObjects.Graphics) {
     // super(); 
      this.graphics = graphics;
      
    }

    setGameObject(game:GameScene | GameSceneExtra):void{
        this.game = game;
    }

    setConf(r:number,stroke:number):void{
        this.radius = r;
        this.stroke = stroke;
    }


    drawBoard9x9(board:string[][],delimetr:number,fill:Border): void {
        this.graphics.beginPath();
        this.graphics.lineStyle(3, 0xffffff, 1);
        this.graphics.moveTo(400, 300);
        this.graphics.lineTo(400, 600);
        this.graphics.moveTo(500, 300);
        this.graphics.lineTo(500, 600);
        this.graphics.moveTo(300, 400);
        this.graphics.lineTo(600, 400);
        this.graphics.moveTo(300, 500);
        this.graphics.lineTo(600, 500);
        this.graphics.closePath();
        this.graphics.strokePath();
        this.drowOldshapes(board,delimetr,fill);
        ////////////////////////////////
        const graphics_line =  this.game.add.graphics();
        graphics_line.alpha = 0;
        graphics_line.beginPath();
        graphics_line.lineStyle(3, 0xffffff, 1);
        for (let i = 1; i < 9; i++) {
            graphics_line.moveTo(15, i * 100);
            graphics_line.lineTo(885, i * 100);
        }
        for (let j = 1; j < 9; j++) {
            graphics_line.moveTo(j * 100, 15);
            graphics_line.lineTo(j * 100, 885);
        }
        graphics_line.closePath();
        graphics_line.strokePath();
        this.game.tweens.add({
            targets: graphics_line,
            alpha: 1,
            duration: 2500,
            ease: 'Linear'
        });
    }

    drowOldshapes(board:string[][],delimetr:number,fill:Border): void {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (board[3 + i][3 + j] == 'x') {
                    this.drawX(3 + i, 3 + j,delimetr,fill)
                }
                if (board[3 + i][3 + j] == 'o') {
                    this.drawO(3 + i, 3 + j,delimetr,fill)
                }
            }
        }
    }


           //graphics
        drawBoard(col_1:Line,col_2:Line,row_1:Line,row_2:Line): void {
            this.graphics.beginPath();
            this.graphics.lineStyle(3, 0xffffff, 1);
            this.graphics.moveTo(col_1.x0, col_1.y0);
            this.graphics.lineTo(col_1.x1, col_1.y1);
            this.graphics.moveTo(col_2.x0, col_2.y0);
            this.graphics.lineTo(col_2.x1, col_2.y1);
            this.graphics.moveTo(row_1.x0, row_1.y0);
            this.graphics.lineTo(row_1.x1, row_1.y1);
            this.graphics.moveTo(row_2.x0, row_2.y0);
            this.graphics.lineTo(row_2.x1, row_2.y1);
            this.graphics.closePath();
            this.graphics.strokePath();
        }
        drawX(i: number, j: number,delimetr:number, fill:Border): void {
            const line1 = this.game.add.line(0, 0, 0, 0, 0, 0, 0xd50102);
            line1.setTo((15 + j * delimetr) + fill.x, (15 + i * delimetr) + fill.y, ((j + 1) * delimetr - 15) + fill.x, ((i + 1) * delimetr - 15) + fill.y)
            line1.setLineWidth(12);
            const line = this.game.add.line(0, 0, 0, 0, 0, 0, 0xd50102);
            line.setLineWidth(12);
            const startPoint = {
                x: ((j + 1) * delimetr - 15) + fill.x,
                y: (15 + i * delimetr) + fill.y
            };
            const endPoint = {
                x: ((j + 1) * delimetr - 15) + fill.x,
                y: (15 + i * delimetr) + fill.y
            };
            line.setTo(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
            this.game.tweens.add({
                targets: endPoint,
                x: (15 + j * delimetr) + fill.x,
                y: ((i + 1) * delimetr - 15) + fill.y,
                duration: 200,
                onUpdate: () => {
                    line.setTo(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
                },
                onComplete: () => {}
            });
        }
        drawO(i: number, j: number,delimetr:number, fill:Border): void {
            let centerX = ((j * delimetr + (j + 1) * delimetr) / 2) + fill.x
            let centerY = ((i * delimetr + (i + 1) * delimetr) / 2) + fill.y
            //let radius = 85
            const startAngle = {
                angle: 0
            };
            const endAngle = {
                angle: 0
            };
            const arc = this.game.add.arc(centerX, centerY, this.radius, startAngle.angle, endAngle.angle, false);
            arc.setStrokeStyle(this.stroke, 0xa6b401);
            arc.closePath = false;
            arc.setEndAngle(Phaser.Math.DegToRad(endAngle.angle));
            this.game.tweens.add({
                targets: endAngle,
                angle: 360,
                duration: 200,
                onUpdate: () => {
                    arc.setEndAngle(endAngle.angle);
                },
                onComplete: () => {}
            });
        }
        drowLine(i_0: number, j_0: number, i_end: number, j_end: number, color: number,delimetr:number,fill:Border): void {
            const line = this.game.add.line(0, 0, 0, 0, 0, 0, color);
            line.setLineWidth(4);
            const startPoint = {
                x: j_0 * delimetr + delimetr/2 + fill.x,
                y: i_0 * delimetr + delimetr/2 + fill.y
            };
            const endPoint = {
                x: j_0 * delimetr + delimetr/2 + fill.x,
                y: i_0 * delimetr + delimetr/2+ fill.y
            };
            line.setTo(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
            this.game.tweens.add({
                targets: endPoint,
                x: j_end * delimetr + delimetr/2 + fill.x,
                y: i_end * delimetr + delimetr/2 + fill.y,
                duration: 2000,
                onUpdate: () => {
                    line.setTo(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
                },
                onComplete: () => {}
            });
        }

}