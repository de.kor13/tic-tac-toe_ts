import { Graph_board } from './drawManager';
import { Border} from './game-scene';

export class BotContoller9x9 {
    constructor(){

    }
    private board:string[][]
    setBoard(board:string[][]){
        this.board = board;
    }

    checkWinner(graph_board: Graph_board,delimetr:number,fill:Border): string {
        // Horizontally check
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 5; j++) {
                if (this.board[i][j] === this.board[i][j + 1] && this.board[i][j] === this.board[i][j + 2] && this.board[i][j] === this.board[i][j + 3] && this.board[i][j] === this.board[i][j + 4]) {
                    if (this.board[i][j] === 'x') {
                        graph_board.drowLine(i, j, i, j + 4, 0xd50102,delimetr,fill)
                        return 'x';
                    } else if (this.board[i][j] === 'o') {
                        graph_board.drowLine(i, j, i, j + 4, 0xa6b401,delimetr,fill)
                        return 'o';
                    }
                }
            }
        }
        // Vertically check
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 9; j++) {
                if (this.board[i][j] == this.board[i + 1][j] && this.board[i][j] == this.board[i + 2][j] && this.board[i][j] == this.board[i + 3][j] && this.board[i][j] == this.board[i + 4][j]) {
                    if (this.board[i][j] === 'x') {
                        graph_board.drowLine(i, j, i + 4, j, 0xd50102,delimetr,fill)
                        return 'x';
                    } else if (this.board[i][j] === 'o') {
                        graph_board.drowLine(i, j, i + 4, j, 0xa6b401,delimetr,fill)
                        return 'o';
                    }
                }
            }
        }
        // Diagonally check
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                if (this.board[i][j] == this.board[i + 1][j + 1] && this.board[i][j] == this.board[i + 2][j + 2] && this.board[i][j] == this.board[i + 3][j + 3] && this.board[i][j] == this.board[i + 4][j + 4]) {
                    if (this.board[i][j] === 'x') {
                        graph_board.drowLine(i, j, i + 4, j + 4, 0xd50102,delimetr,fill)
                        return 'x';
                    } else if (this.board[i][j] === 'o') {
                        graph_board.drowLine(i, j, i + 4, j + 4, 0xa6b401,delimetr,fill)
                        return 'o';
                    }
                }
            }
        }
        for (let i = 0; i < 5; i++) {
            for (let j = 4; j < 9; j++) {
                if (this.board[i][j] == this.board[i + 1][j - 1] && this.board[i][j] == this.board[i + 2][j - 2] && this.board[i][j] == this.board[i + 3][j - 3] && this.board[i][j] == this.board[i + 4][j - 4]) {
                    if (this.board[i][j] === 'x') {
                        graph_board.drowLine(i, j, i + 4, j - 4, 0xd50102,delimetr,fill)
                        return 'x';
                    } else if (this.board[i][j] === 'o') {
                        graph_board.drowLine(i, j, i + 4, j - 4, 0xa6b401,delimetr,fill)
                        return 'o';
                    }
                }
            }
        }
        return '';
    }

    isFinished(): boolean {
        // Horizontally check
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 5; j++) {
                if (this.board[i][j] == this.board[i][j + 1] && this.board[i][j] == this.board[i][j + 2] && this.board[i][j] == this.board[i][j + 3] && this.board[i][j] == this.board[i][j + 4]) {
                    if (this.board[i][j] == 'x') return true
                    else if (this.board[i][j] == 'o') return true
                }
            }
        }
        // Vertically check
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 9; j++) {
                if (this.board[i][j] == this.board[i + 1][j] && this.board[i][j] == this.board[i + 2][j] && this.board[i][j] == this.board[i + 3][j] && this.board[i][j] == this.board[i + 4][j]) {
                    if (this.board[i][j] == 'x') return true
                    else if (this.board[i][j] == 'o') return true
                }
            }
        }
        // Diagonally check
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                if (this.board[i][j] == this.board[i + 1][j + 1] && this.board[i][j] == this.board[i + 2][j + 2] && this.board[i][j] == this.board[i + 3][j + 3] && this.board[i][j] == this.board[i + 4][j + 4]) {
                    if (this.board[i][j] == 'x') return true
                    else if (this.board[i][j] == 'o') return true
                }
            }
        }
        for (let i = 0; i < 5; i++) {
            for (let j = 4; j < 9; j++) {
                if (this.board[i][j] == this.board[i + 1][j - 1] && this.board[i][j] == this.board[i + 2][j - 2] && this.board[i][j] == this.board[i + 3][j - 3] && this.board[i][j] == this.board[i + 4][j - 4]) {
                    if (this.board[i][j] == 'x') return true
                    else if (this.board[i][j] == 'o') return true
                }
            }
        }
        return this.noMoveLeft(this.board)
    }

    evaluation(board: string[][], player: string): number {
        let opponent = player == 'x' ? 'o' : 'x';
        // Horizontally check
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 5; j++) {
                if (board[i][j] == board[i][j + 1] && board[i][j] == board[i][j + 2] && board[i][j] == board[i][j + 3] && board[i][j] == board[i][j + 4]) {
                    if (board[i][j] == player) return +10;
                    else if (board[i][j] == opponent) return -10;
                }
            }
        }
        // Vertically check
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 9; j++) {
                if (board[i][j] == board[i + 1][j] && board[i][j] == board[i + 2][j] && board[i][j] == board[i + 3][j] && board[i][j] == board[i + 4][j]) {
                    if (board[i][j] == player) return +10;
                    else if (board[i][j] == opponent) return -10;
                }
            }
        }
        // Diagonally check
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                if (board[i][j] == board[i + 1][j + 1] && board[i][j] == board[i + 2][j + 2] && board[i][j] == board[i + 3][j + 3] && board[i][j] == board[i + 4][j + 4]) {
                    if (board[i][j] == player) return +10;
                    else if (board[i][j] == opponent) return -10;
                }
            }
        }
        for (let i = 0; i < 5; i++) {
            for (let j = 4; j < 9; j++) {
                if (board[i][j] == board[i + 1][j - 1] && board[i][j] == board[i + 2][j - 2] && board[i][j] == board[i + 3][j - 3] && board[i][j] == board[i + 4][j - 4]) {
                    if (board[i][j] == player) return +10;
                    else if (board[i][j] == opponent) return -10;
                }
            }
        }
        return 0;
    }

    miniMax(board: string[][], depth: number, alpha: number, beta: number, isMax: boolean, player: string, maxDepth: number): number {
        let opponent = player == 'x' ? 'o' : 'x';
        let score = this.evaluation(board, player);
        if (score == 10) {
            return score - depth;
        }
        if (score == -10) {
            return score + depth;
        }
        if (this.noMoveLeft(board) || depth == maxDepth) {
            return 0;
        }
        if (isMax) {
            let value = -Infinity;
            for (let i = 0; i < 9; i++) {
                for (let j = 0; j < 9; j++) {
                    if (board[i][j] == '_') {
                        board[i][j] = player;
                        value = Math.max(value, this.miniMax(board, depth + 1, alpha, beta, !isMax, player, maxDepth));
                        board[i][j] = '_';
                        alpha = Math.max(alpha, value);
                        if (alpha >= beta) {
                            return value;
                        }
                    }
                }
            }
            return value;
        } else {
            let value = Infinity;
            for (let i = 0; i < 9; i++) {
                for (let j = 0; j < 9; j++) {
                    if (board[i][j] == '_') {
                        board[i][j] = opponent;
                        value = Math.min(value, this.miniMax(board, depth + 1, alpha, beta, !isMax, player, maxDepth));
                        board[i][j] = '_';
                        beta = Math.min(beta, value);
                        if (beta <= alpha) {
                            return value;
                        }
                    }
                }
            }
            return value;
        }
    }

    findBestMove(board: string[][], player: string): {
        row: number,
        col: number
    } {
        let score: number = -1000;
        let move: {
            row: number,
            col: number
        } = {
            row: -1,
            col: -1
        };
        const midRow = Math.floor(board.length / 2);
        const midCol = Math.floor(board[0].length / 2);
        for (let i = 0; i < board.length; i++) {
            for (let j = 0; j < board[0].length; j++) {
                if (i < midRow - 2 || i > midRow + 2 || j < midCol - 2 || j > midCol + 2) {
                    if (board[i][j] == '_') {
                        board[i][j] = player;
                        let moveScore = this.miniMax(board, 0, -Infinity, Infinity, false, player, 2);
                        board[i][j] = '_';
                        if (moveScore >= score) {
                            move.row = i;
                            move.col = j;
                            score = moveScore;
                        }
                    }
                }
            }
        }
        for (let i = midRow - 2; i <= midRow + 2; i++) {
            for (let j = midCol - 2; j <= midCol + 2; j++) {
                if (board[i][j] == '_') {
                    board[i][j] = player;
                    let moveScore = this.miniMax(board, 0, -Infinity, Infinity, false, player, 2);
                    board[i][j] = '_';
                    if (moveScore >= score) {
                        move.row = i;
                        move.col = j;
                        score = moveScore;
                    }
                }
            }
        }
        return move;
    }

    noMoveLeft(board: string[][]): boolean {
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9; j++) {
                if (board[i][j] == '_') return false
            }
        }
        return true
    }

  }