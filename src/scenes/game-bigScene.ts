import { TurnManager} from './logic';
import { BotContoller9x9} from './bot9x9';


type Line = {
    x0: number;
    y0: number;
    x1:number;
    y1:number
  }
  type Border = {
    x:number;
    y:number;
  }

export class GameSceneExtra extends Phaser.Scene {
    private board: string[][];
    private graphics!: Phaser.GameObjects.Graphics;
    private delimetr: number = 100;
    private turnManager:TurnManager;
    private botContoller:BotContoller9x9

    private fill:Border ={
        x: 0,
        y: 0   
}

    constructor() {
        super({
            key: 'gameScene-second'
        })
    }
    preload(): void {}
    
    create(data: {
        player: string,
        board: string[][],
        turn: string
    }): void {
        this.graphics = this.add.graphics();
        this.board = [
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
            ['_', '_', '_', '_', '_', '_', '_', '_', '_'],
        ];
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                this.board[i + 3][j + 3] = data.board[i][j];
            }
        }



  
        this.botContoller= new BotContoller9x9();
        this.botContoller.setBoard(this.board)

        this.turnManager = new TurnManager(this,this.graphics);
        this.turnManager.setBot(this.botContoller);
        this.turnManager.setDelimetr(this.delimetr)
        this.turnManager.setFill(this.fill)
        //this.turnManager.setLine(this.col_1,this.col_2,this.row_1,this.row_2)
        this.turnManager.start9x9(this.board,data,false)
        this.turnManager.drowBoard9x9();
        




    }

 
}