export class EndGame extends Phaser.Scene {
    private message: string;
    
    constructor() {
        super({key: 'endGame'});
    }



    create(data:{message: string}):void {
        this.add.text(100, 100, data.message, {fontSize: "150px"});
        let rematch = this.add.text(100, 400, 'Rematch', {fontSize: "150px"});
        rematch.setInteractive();

        rematch.on('pointerover', () => {
            rematch.setColor('#eff67b');
        });
        rematch.on('pointerout', () => {
            rematch.setColor('#ffffff');
        });
        rematch.on('pointerdown', () => this.rematch());
    }

     rematch():void {
        this.scene.start('startGame');
    }
}