
export class StartGame extends Phaser.Scene{
	constructor(){
		super({key: 'startGame'})
	}

	create(): void {

		let x = this.add.text(250, 300, 'X', {fontSize: "200px" })
		let o = this.add.text(500, 300, 'O', {fontSize:"200px" })

		x.setInteractive(new Phaser.Geom.Rectangle(0, 0,  x.width, x.height),
			Phaser.Geom.Rectangle.Contains)
		o.setInteractive(new Phaser.Geom.Rectangle(0, 0,  o.width, o.height),
			Phaser.Geom.Rectangle.Contains)

		x.on('pointerover', function(){
			x.setColor('#d50102')
		})
		x.on('pointerout', function (){
			x.setColor("#ffffff")
		})
		x.on('pointerdown', function(){
			this.scene.start('gameScene', {player: 'x'})
		}, this)
		o.on('pointerover', function(){
			o.setColor("#a6b401")
		})
		o.on('pointerout', function (){
			o.setColor("#ffffff")
		})
		o.on('pointerdown', function(){
			this.scene.start('gameScene', {player: 'o'})
		}, this)
	}
}
