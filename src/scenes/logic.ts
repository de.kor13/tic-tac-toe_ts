import { GameScene, Line, Border} from './game-scene';
import { GameSceneExtra } from './game-bigScene';
import { Graph_board } from './drawManager';


  
import { BotContoller3x3} from './bot3x3';
import { BotContoller9x9} from './bot9x9';



export class TurnManager{
    //graphics: Phaser.GameObjects.Graphics;

    private event = new Phaser.Events.EventEmitter();
    private board: string[][];
    private gameStatus: number;
    private player: string;
    private computer: string;
    private filling: number;
    private turn: string;
    private bot:BotContoller3x3 | BotContoller9x9

    private graph_board: Graph_board;

    private graphics: Phaser.GameObjects.Graphics
  
    private game: GameScene | GameSceneExtra;
    private delimetr: number;
    private smallFill:boolean

    private col_1:Line 
    private col_2:Line 
    private row_1:Line 
    private row_2:Line 
    private fill:Border 

    constructor(game: GameScene | GameSceneExtra,graphics: Phaser.GameObjects.Graphics){
        this.game = game;
        this.graphics = graphics;
    }
    setDelimetr(delimetr: number){
        this.delimetr = delimetr;        
    }

    setLine(col_1:Line,col_2:Line,row_1:Line,row_2:Line){
        this.col_1 = col_1
        this.col_2 = col_2
        this.row_1 = row_1
        this.row_2 = row_2
    }
    setFill(fill:Border){
        this.fill = fill;
    }
    setBot(bot:BotContoller3x3 | BotContoller9x9){
        this.bot = bot
    }



    drowBoard(){
        this.graph_board.drawBoard(this.col_1,this.col_2,this.row_1,this.row_2)
    }
    drowBoard9x9(){
        this.graph_board.drawBoard9x9(this.board,this.delimetr,this.fill)

    }
    
    start9x9(board:string[][],data: {player: string,board: string[][],turn: string},smallFill:boolean):void{
        this.board=board;
        this.graphics = this.game.add.graphics();
        this.graph_board = new Graph_board(this.graphics)
        this.graph_board.setGameObject(this.game)
        this.graph_board.setConf(40,12)

        this.smallFill = smallFill;
        
        this.event.on('changeTurn', this.changeTurn, this);
        this.event.on('gameFinished', this.gameFinished, this);
        this.player = data.player;
        this.computer = data.player === 'x' ? 'o' : 'x';
        this.turn = data.turn;

        this.event.emit('changeTurn', this.turn);


        

    }


    start(board:string[][],data:{player: string},smallFill:boolean):void{
        this.board=board;
        this.graphics = this.game.add.graphics();
        this.graph_board = new Graph_board(this.graphics)
        this.graph_board.setGameObject(this.game)
        this.graph_board.setConf(85,24)
        this.smallFill = smallFill;


        this.filling = 0;

        
        this.event.on('changeTurn', this.changeTurn, this);
        this.event.on('gameFinished', this.gameFinished, this);
        this.player = data.player;
        this.computer = data.player === 'x' ? 'o' : 'x';
        console.log(Math.floor(Math.random() * 2));
        if (Math.floor(Math.random() * 2) === 0) {
            this.turn = this.player;
        } else {
            this.turn = this.computer;
        }

     //   this.graph_board.drawBoard(this.col_1,this.col_2,this.row_1,this.row_2)
        //this.drawBoard();
        this.event.emit('changeTurn', this.turn);

    }



    changeTurn(turn: string): void {
        if (this.bot.isFinished()) {
            this.event.emit('gameFinished')
        } else {
            if (turn == this.computer) {
                this.computerTurn()
            } else if (turn == this.player) {
                this.playerTurn()
            }
        }
    }
    checkFilling(): boolean {
        if (this.filling == 6) return true
        else return false
    }
    computerTurn(): void {
        if (!this.bot.isFinished() && this.turn == this.computer) {
            if(this.smallFill === true){
                this.filling++;
            if (this.checkFilling()) {
                console.log("70%")
                this.game.scene.start('gameScene-second', {
                    player: this.player,
                    board: this.board,
                    turn: this.turn
                });
            }
        }
            let move = this.bot.findBestMove(this.board, this.computer)
            this.board[move.row][move.col] = this.computer
           
            if (this.computer == 'x') {
                this.graph_board.drawX(move.row, move.col,this.delimetr,this.fill)
            } else if (this.computer == 'o') {
                this.graph_board.drawO(move.row, move.col,this.delimetr,this.fill)
            }
            this.turn = this.player
            this.event.emit('changeTurn', this.player)
        }
    }
    playerTurn(): void {
        let game = this
        if(this.smallFill === true){
            game.filling++;
        if (game.checkFilling()) {
            console.log("70%")
            game.game.scene.start('gameScene-second', {
                player: this.player,
                board: this.board,
                turn: this.turn
            });
        }
    }
        if (game.game.input.enabled == true) {
            game.game.input.on('pointerdown', function(pointer: Phaser.Input.Pointer) {
                if (!game.bot.isFinished() && game.turn == game.player) {
                    if (pointer.x > game.fill.x && pointer.x < 900 - game.fill.x && pointer.y > game.fill.y && pointer.y < 900 - game.fill.y) {
                        let move = game.determineCoordinate(pointer.x, pointer.y)
                        if (game.board[move.row][move.col] == '_') {
                            game.board[move.row][move.col] = game.player
                            
                            if (game.player == 'x') {
                                
                                game.graph_board.drawX(move.row, move.col,game.delimetr,game.fill)
                            } else if (game.player == 'o') {
                                game.graph_board.drawO(move.row, move.col,game.delimetr,game.fill)
                            }
                            game.turn = game.computer
                            game.event.emit('changeTurn', game.computer)
                        }
                    }
                }
            })
        }
    }
    determineCoordinate(x: number, y: number): {
        row: number,
        col: number
    } {
        let i: number = Math.floor((y - (15 + this.fill.y)) / this.delimetr);
        let j: number = Math.floor((x - (15 + this.fill.y)) / this.delimetr);
        return {
            row: i,
            col: j
        };
    }
   
    showEndScene(): void {
        let message: string = '';
        if (this.gameStatus === 1) {
            message = 'YOU WON!';
        } else if (this.gameStatus === -1) {
            message = 'YOU LOST!';
        } else {
            message = 'TIE!';
        }
        this.game.scene.start('endGame', {
            message
        });
    }
    gameFinished(): void {
        let winner: string = this.bot.checkWinner(this.graph_board,this.delimetr,this.fill);
        if (winner === this.player) {
            this.gameStatus = 1;
        } else if (winner === this.computer) {
            this.gameStatus = -1;
        } else {
            this.gameStatus = 0;
        }
        this.game.time.delayedCall(3000, this.showEndScene, [], this);
    }


    
   
}



