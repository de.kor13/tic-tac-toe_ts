import { Graph_board } from './drawManager';
import { Border} from './game-scene';

  export class BotContoller3x3 {
    constructor(){

    }
    private board:string[][]
    setBoard(board:string[][]){
        this.board = board;

    }

    checkWinner(graph_board: Graph_board,delimetr:number,fill:Border): string {
        // Horizontally check
        for (let row = 0; row < 3; row++) {
            if (this.board[row][0] === this.board[row][1] && this.board[row][1] === this.board[row][2]) {
                if (this.board[row][0] === 'x') {
                    graph_board.drowLine(row, 0, row, 2, 0xd50102,delimetr,fill)
                    return 'x';
                } else if (this.board[row][0] === 'o') {
                    graph_board.drowLine(row, 0, row, 2, 0xa6b401,delimetr,fill)
                    return 'o';
                }
            }
        }
        // Vertically check
        for (let col = 0; col < 3; col++) {
            if (this.board[0][col] === this.board[1][col] && this.board[1][col] === this.board[2][col]) {
                if (this.board[0][col] === 'x') {
                    graph_board.drowLine(0, col, 2, col, 0xd50102,delimetr,fill)
                    return 'x';
                } else if (this.board[0][col] === 'o') {
                    graph_board.drowLine(0, col, 2, col, 0xa6b401,delimetr,fill)
                    return 'o';
                }
            }
        }
        // Diagonally check
        if (this.board[0][0] === this.board[1][1] && this.board[1][1] === this.board[2][2]) {
            if (this.board[1][1] === 'x') {
                graph_board.drowLine(0, 0, 2, 2, 0xd50102, delimetr,fill)
                return 'x';
            } else if (this.board[1][1] === 'o') {
                graph_board.drowLine(0, 0, 2, 2, 0xa6b401,delimetr,fill)
                return 'o';
            }
        }
        if (this.board[2][0] === this.board[1][1] && this.board[1][1] === this.board[0][2]) {
            if (this.board[1][1] === 'x') {
                graph_board.drowLine(2, 0, 0, 2, 0xd50102,delimetr,fill)
                return 'x';
            } else if (this.board[1][1] === 'o') {
                graph_board.drowLine(2, 0, 0, 2, 0xa6b401,delimetr,fill)
                return 'o';
            }
        }
        return '';
    }

    isFinished(): boolean {
        // Horizontally check
        for (let row = 0; row < 3; row++) {
            if (this.board[row][0] == this.board[row][1] && this.board[row][1] == this.board[row][2]) {
                if (this.board[row][0] == 'x') return true
                else if (this.board[row][0] == 'o') return true
            }
        }
        // Vertically check
        for (let col = 0; col < 3; col++) {
            if (this.board[0][col] == this.board[1][col] && this.board[1][col] == this.board[2][col]) {
                if (this.board[0][col] == 'x') return true
                else if (this.board[0][col] == 'o') return true
            }
        }
        // Diagonally check
        if (this.board[0][0] == this.board[1][1] && this.board[1][1] == this.board[2][2]) {
            if (this.board[0][0] == 'x') return true
            else if (this.board[0][0] == 'o') return true
        }
        if (this.board[2][0] == this.board[1][1] && this.board[1][1] == this.board[0][2]) {
            if (this.board[1][1] == 'x') return true
            else if (this.board[1][1] == 'o') return true
        }
        // Check no move left
        return this.noMoveLeft(this.board)
    }
    evaluation(board: string[][], player: string): number {
        let opponent = player == 'x' ? 'o' : 'x';
        // Horizontally check
        for (let row = 0; row < 3; row++) {
            if (board[row][0] == board[row][1] && board[row][1] == board[row][2]) {
                if (board[row][0] == player) return +10;
                else if (board[row][0] == opponent) return -10;
            }
        }
        // Vertically check
        for (let col = 0; col < 3; col++) {
            if (board[0][col] == board[1][col] && board[1][col] == board[2][col]) {
                if (board[0][col] == player) return +10;
                else if (board[0][col] == opponent) return -10;
            }
        }
        // Diagonally check
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            if (board[1][1] == player) return +10;
            else if (board[1][1] == opponent) return -10;
        }
        if (board[2][0] == board[1][1] && board[1][1] == board[0][2]) {
            if (board[1][1] == player) return +10;
            else if (board[1][1] == opponent) return -10;
        }
        return 0;
    }
    noMoveLeft(board: string[][]): boolean {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (board[i][j] == '_') return false
            }
        }
        return true
    }
    miniMax(board: string[][], depth: number, isMax: boolean, player: string): number {
        let opponent = player == 'x' ? 'o' : 'x';
        let score = this.evaluation(board, player);
        if (score == 10) {
            return score - depth;
        }
        if (score == -10) {
            return score + depth;
        }
        if (this.noMoveLeft(board)) {
            return 0;
        }
        if (isMax) {
            let value = -1000;
            for (let i = 0; i < 3; i++) {
                for (let j = 0; j < 3; j++) {
                    if (board[i][j] == '_') {
                        board[i][j] = player;
                        value = Math.max(value, this.miniMax(board, depth + 1, !isMax, player));
                        board[i][j] = '_';
                    }
                }
            }
            return value;
        } else {
            let value = 1000;
            for (let i = 0; i < 3; i++) {
                for (let j = 0; j < 3; j++) {
                    if (board[i][j] == '_') {
                        board[i][j] = opponent;
                        value = Math.min(value, this.miniMax(board, depth + 1, !isMax, player));
                        board[i][j] = '_';
                    }
                }
            }
            return value;
        }
    }
    findBestMove(board: string[][], player: string): {
            row: number,
            col: number
        } {
            let score: number = -1000;
            let move: {
                row: number,
                col: number
            } = {
                row: -1,
                col: -1
            };
            for (let i = 0; i < 3; i++) {
                for (let j = 0; j < 3; j++) {
                    if (board[i][j] == '_') {
                        board[i][j] = player;
                        let moveScore = this.miniMax(board, 0, false, player);
                        board[i][j] = '_';
                        if (moveScore >= score) {
                            move.row = i;
                            move.col = j;
                            score = moveScore;
                        }
                    }
                }
            }
            return move;
        }


}