
import { TurnManager,} from './logic';
import { BotContoller3x3} from './bot3x3';
import { BotContoller9x9} from './bot9x9';

export type Line = {
    x0: number;
    y0: number;
    x1:number;
    y1:number
  }
  export type Border = {
    x:number;
    y:number;
  }




export class GameScene extends Phaser.Scene {
   
    private board: string[][];
    private graphics!: Phaser.GameObjects.Graphics;
    private delimetr: number = 200;
    private turnManager:TurnManager;
    private botContoller:BotContoller3x3
    

    private col_1:Line ={
        x0: 350,
        y0: 150,
        x1: 350,
        y1: 750 
    } 
    private col_2:Line ={
        x0: 550,
        y0: 150,
        x1: 550,
        y1: 750
    }
    private row_1:Line ={
        x0: 150,
        y0: 350,
        x1: 750,
        y1: 350
    } 
    private row_2:Line ={
        x0: 150,
        y0: 550,
        x1: 750,
        y1: 550
    }
    private fill:Border ={
            x: 150,
            y: 150   
    }

    constructor() {
        super({
            key: 'gameScene'
        })
    }
    preload(): void {}
    create(data: {
        player: string
    }): void {
        this.board = [
            ['_', '_', '_'],
            ['_', '_', '_'],
            ['_', '_', '_'],
        ];

        this.graphics = this.add.graphics();
        this.botContoller= new BotContoller3x3();
        this.botContoller.setBoard(this.board)
        this.turnManager = new TurnManager(this,this.graphics);
        this.turnManager.setBot(this.botContoller);
        this.turnManager.setDelimetr(this.delimetr)
        this.turnManager.setFill(this.fill)
        this.turnManager.setLine(this.col_1,this.col_2,this.row_1,this.row_2)
        this.turnManager.start(this.board,data,true)
        this.turnManager.drowBoard();
        


        
      
   


 
    }
 

}