

import 'phaser';
import { StartGame } from './scenes/startGame';
import { GameScene } from './scenes/game-scene';
import { GameSceneExtra } from './scenes/game-bigScene';
import { EndGame } from './scenes/endGame';

const config: Phaser.Types.Core.GameConfig = {
  title: 'tic-tac-toe',
  version: '1.0',
	type: Phaser.AUTO,
	width: 900,
	height: 900,
	autoCenter: Phaser.Scale.CENTER_BOTH,
	backgroundColor: "#8000ff",
  scene: [StartGame, GameScene, GameSceneExtra, EndGame]
};

export class Game extends Phaser.Game {
  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);
  }
}

window.addEventListener('load', () => {
  var game = new Game(config);
});


